# from redditsearch.reddit.utils import get_reddit_search
# from requests.exceptions import ReadTimeout
# from rest_framework import status
# from celery import shared_task
#
#
# @shared_task(bind=True)
# def get_reddit(data_type, query, duration, size, sort_type, sort, aggs, score, num_comments, fields):
#     try:
#         response = get_reddit_search(data_type=data_type,
#                                      q=query,
#                                      after=duration,
#                                      size=size,
#                                      sort_type=sort_type,
#                                      sort=sort,
#                                      aggs=aggs,
#                                      score=score,
#                                      num_comments=num_comments,
#                                      )
#         response['status_code'] = status.HTTP_200_OK
#         response['message'] = 'Successfully Fetched'
#     except ReadTimeout:
#         response = {
#             'status_code': status.HTTP_408_REQUEST_TIMEOUT,
#             'message': 'Error',
#             'errors': [
#                 'Request timed out'
#             ]
#         }
#     return response
