import requests


def get_reddit_search(data_type, **kwargs):
    base_url = f"https://api.pushshift.io/reddit/search/{data_type}/"
    payload = kwargs
    request = requests.get(base_url, params=payload)
    return request.json()


def get_info(res):
    if res['status_code'] == 200:
        i = 0
