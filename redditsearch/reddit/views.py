from django.http import HttpResponse, HttpResponseNotFound, JsonResponse
from tasks import get_reddit
from celery.result import AsyncResult


def index(request):
    return HttpResponse("Hello, world. You're at the reddit index.")


def quick_search(request):
    if request.method == 'GET':
        if request.GET.get('query'):
            # import pdb
            # pdb.set_trace()
            data_type = request.GET.get('data_type', 'comment')
            query = request.GET.get('query')
            duration = request.GET.get('duration', '30d')
            size = request.GET.get('size', '10')
            sort_type = request.GET.get('sort_type', 'score')  # Sort by score (Accepted: "score", "num_comments",
            # "created_utc")
            sort = request.GET.get('sort', 'desc')  # sort descending
            aggs = request.GET.get('aggs', '')  # "author", "link_id", "created_utc", "subreddit"
            score = request.GET.get('score', 'score=>0')
            num_comments = request.GET.get('num_comments', 'num_comments=>0')
            fields = 'body'
            res = get_reddit.delay(data_type, query, duration, size, sort_type, sort, aggs, score, num_comments, fields)
            task = AsyncResult(res.id)
            response = task.get()
            return JsonResponse(response)
        else:
            return HttpResponseNotFound("query is not defined")
    else:
        return HttpResponseNotFound('Not allowed for current method')
