from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('quick_search', views.quick_search, name='quick_search')
]
