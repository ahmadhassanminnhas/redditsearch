import os

from celery import Celery
from redditsearch.reddit.utils import get_reddit_search
from requests.exceptions import ReadTimeout
from rest_framework import status

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'redditsearch.settings')

app = Celery('redditsearch', backend='redis://localhost', broker='redis://localhost')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
