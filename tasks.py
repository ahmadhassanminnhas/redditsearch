from celery import Celery
from redditsearch.reddit.utils import get_reddit_search
from requests.exceptions import ReadTimeout
from rest_framework import status

app = Celery('tasks', backend='redis://localhost', broker='redis://localhost')


@app.task
def add(x, y):
    return x + y


@app.task
def get_reddit(data_type, query, duration, size, sort_type, sort, aggs, score, num_comments, fields):
    try:
        response = get_reddit_search(data_type=data_type,
                                     q=query,
                                     after=duration,
                                     size=size,
                                     sort_type=sort_type,
                                     sort=sort,
                                     aggs=aggs,
                                     score=score,
                                     num_comments=num_comments,
                                      fields=fields
                                     )
        response['status_code'] = status.HTTP_200_OK
        response['message'] = 'Successfully Fetched'
    except ReadTimeout:
        response = {
            'status_code': status.HTTP_408_REQUEST_TIMEOUT,
            'message': 'Error',
            'errors': [
                'Request timed out'
            ]
        }
    return response
